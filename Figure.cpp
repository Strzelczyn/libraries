#include "Figure.h"

Figure::Figure() : m_surfaceArea(0), m_Perimeter(0) {}

int Figure::getsurfaceArea() {
  return m_surfaceArea;
}

int Figure::getPerimeter() {
  return m_Perimeter;
}
