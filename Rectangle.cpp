#include "Rectangle.h"

Rectangle::Rectangle() : m_sideA(0), m_sideB(0) {}

Rectangle::Rectangle(int sideA, int sideB) : m_sideA(sideA), m_sideB(sideB) {}

void Rectangle::surfaceArea() {
  m_surfaceArea = m_sideA * m_sideB;
}
void Rectangle::Perimeter() {
  m_Perimeter = 2 * m_sideA + 2 * m_sideB;
}